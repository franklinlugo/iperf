#! /bin/bash
#
# Script para hacer pruebas de rendimiento y ancho de banda a una red
#
# Autor: Franklin Lugo


#midiendo el rendimiento del enlace
echo "Midiendo el rendimiento del enlace"
echo -n "introduzca la ip de destino: "; read ip
iperf -c $ip -u -P 1 -i 1 -p 5001 -f m -b 100.0M -t 10 -r -L 5001 -T 1 > rendimiento_client_side.txt
echo "Prueba de rendimiento del enlace realizada"