#! /bin/bash

echo -n "introduzca la ip para hacer ping: "; read ip
echo "Haciendo ping a $ip"
ping -c 30 $ip > ping_to_server.txt

#con la opcion -c se le especifica la cantidad de paquetes a transmitir

echo "Haciendo ping a google.com"
ping -c 30 google.com > ping_to_google.txt