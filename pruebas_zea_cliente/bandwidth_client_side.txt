------------------------------------------------------------
Client connecting to 10.5.9.20, TCP port 5001
TCP window size: 21.0 KByte (default)
------------------------------------------------------------
[  3] local 10.5.9.4 port 39218 connected with 10.5.9.20 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0- 5.0 sec  56.8 MBytes  95.2 Mbits/sec
[  3]  5.0-10.0 sec  40.2 MBytes  67.5 Mbits/sec
[  3] 10.0-15.0 sec  56.1 MBytes  94.2 Mbits/sec
[  3] 15.0-20.0 sec  56.0 MBytes  94.0 Mbits/sec
[  3] 20.0-25.0 sec  56.2 MBytes  94.4 Mbits/sec
[  3] 25.0-30.0 sec  56.1 MBytes  94.2 Mbits/sec
[  3] 30.0-35.0 sec  56.0 MBytes  94.0 Mbits/sec
[  3] 35.0-40.0 sec  56.1 MBytes  94.2 Mbits/sec
[  3] 40.0-45.0 sec  56.1 MBytes  94.2 Mbits/sec
[  3] 45.0-50.0 sec  56.1 MBytes  94.2 Mbits/sec
[  3] 50.0-55.0 sec  56.1 MBytes  94.2 Mbits/sec
[  3] 55.0-60.0 sec  56.1 MBytes  94.2 Mbits/sec
[  3]  0.0-60.0 sec   658 MBytes  92.0 Mbits/sec
