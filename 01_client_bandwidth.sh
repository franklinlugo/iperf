#! /bin/bash
#
# Script para hacer pruebas de rendimiento y ancho de banda a una red
#
# Autor: Franklin Lugo

#midiendo el ancho de banda

echo "Midiendo el ancho de banda"
echo -n "introduzca la ip de destino: "; read ip
iperf -c $ip -t 60 -i 5 > bandwidth_client_side.txt
echo "Prueba de ancho de banda realizada"
