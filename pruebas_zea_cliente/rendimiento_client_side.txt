------------------------------------------------------------
Server listening on UDP port 5001
Receiving 1470 byte datagrams
UDP buffer size: 0.16 MByte (default)
------------------------------------------------------------
------------------------------------------------------------
Client connecting to 10.5.9.20, UDP port 5001
Sending 1470 byte datagrams
UDP buffer size: 0.16 MByte (default)
------------------------------------------------------------
[  4] local 10.5.9.4 port 59589 connected with 10.5.9.20 port 5001
[ ID] Interval       Transfer     Bandwidth
[  4]  0.0- 1.0 sec  11.5 MBytes  96.4 Mbits/sec
[  4]  1.0- 2.0 sec  11.3 MBytes  95.1 Mbits/sec
[  4]  2.0- 3.0 sec  11.4 MBytes  95.4 Mbits/sec
[  4]  3.0- 4.0 sec  11.4 MBytes  96.0 Mbits/sec
[  4]  4.0- 5.0 sec  11.4 MBytes  95.5 Mbits/sec
[  4]  5.0- 6.0 sec  11.4 MBytes  96.0 Mbits/sec
[  4]  6.0- 7.0 sec  6.20 MBytes  52.0 Mbits/sec
[  4]  7.0- 8.0 sec  11.4 MBytes  95.7 Mbits/sec
[  4]  8.0- 9.0 sec  11.4 MBytes  95.7 Mbits/sec
[  4]  0.0-10.0 sec   109 MBytes  91.3 Mbits/sec
[  4] Sent 77677 datagrams
[  4] Server Report:
[  4]  0.0-10.0 sec   109 MBytes  91.2 Mbits/sec   0.047 ms   51/77676 (0.066%)
[  4]  0.0-10.0 sec  1 datagrams received out-of-order
[  3] local 10.5.9.4 port 5001 connected with 10.5.9.20 port 43805
[  3]  0.0- 1.0 sec  11.4 MBytes  95.7 Mbits/sec   0.031 ms    0/ 8140 (0%)
[  3]  1.0- 2.0 sec  11.4 MBytes  95.7 Mbits/sec   0.069 ms    0/ 8138 (0%)
[  3]  2.0- 3.0 sec  11.0 MBytes  92.0 Mbits/sec   0.057 ms  310/ 8137 (3.8%)
[  3]  3.0- 4.0 sec  11.4 MBytes  95.6 Mbits/sec   0.041 ms    9/ 8138 (0.11%)
[  3]  4.0- 5.0 sec  11.4 MBytes  95.7 Mbits/sec   0.038 ms    0/ 8138 (0%)
[  3]  5.0- 6.0 sec  11.4 MBytes  95.7 Mbits/sec   0.268 ms    0/ 8138 (0%)
[  3]  6.0- 7.0 sec  11.4 MBytes  95.7 Mbits/sec   0.042 ms    0/ 8138 (0%)
[  3]  7.0- 8.0 sec  11.4 MBytes  95.7 Mbits/sec   0.146 ms    0/ 8138 (0%)
[  3]  8.0- 9.0 sec  11.4 MBytes  95.7 Mbits/sec   0.049 ms    0/ 8137 (0%)
[  3]  9.0-10.0 sec  11.4 MBytes  95.7 Mbits/sec   0.039 ms    0/ 8138 (0%)
[  3]  0.0-10.0 sec   114 MBytes  95.3 Mbits/sec   0.040 ms  318/81424 (0.39%)
[  3]  0.0-10.0 sec  1 datagrams received out-of-order
